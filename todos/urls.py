from django.urls import path
from todos.views import TodoListListView

path('/todos/', TodoListListView.as_view(), name = "todo_list")